# ROG-X bag parser

MATLAB parser to extract data from .bag log file collected during flight of ROG-X.

The data is saved in a .mat file for postprocessing in MATLAB.

## Getting started

```
git clone https://gitlab.com/ant-x/tools/rog-x_bag_parser.git
cd rog-x_bag_parser
git submodule update --init --recursive
```

Add the following folders to the MATLAB path

```
simulator-toolbox
simulator-toolbox/attitude_library
simulator-toolbox/trajectory_library
```

## Usage

Example: to parse bag file 'X0899_2022-10-27-14-29-32.bag' obtained with drone '/rogx' from the current folder:

```
rosBagParserROGX('X0899_2022-10-27-14-29-32', {'/rogx'})
```

Example: to parse bag file '2021-07-16_10-40-27.bag' containing logged data of drones '/rogx2' and '/rogx3':

```
rosBagParserDrone('2021-07-16_10-40-27', {'/rogx2', '/rogx3'})
```

After parsing the .bag file, plot data from the parsed .mat file: see script `plot_example.m` in the `example` folder.

# Available ROS topics (more to be added):

* `mavros/local_position/pose`: estimated pose from the EKF2 running on the FCU
* `zed2i/zed_node/odom`: odometry data obtained from ZED SDK (type: nav_msgs/Odometry)
* `zed2i/zed_node/pose`: odometry data obtained from ZED SDK (type: geometry_msgs/PoseStamped)
* `mavros/mocap/pose`: pose (position and attitude) measured by mocap

# Notation

The .bag file contains pose information expressed in the ENU frame (consistent with the ROS notation).

In this parser, the information on pose (position and attitude) is converted to the NED frame (North, East, Down).
Thus, the resulting .mat file contains positions expressed in NED and attitude in NED as well (that is, yaw angle is 0 deg when pointing to North and positive clockwise from above).

## Credits
Copyright ANT-X team, 2023

simone@antx.it
