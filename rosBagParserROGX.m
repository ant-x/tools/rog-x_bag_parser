function outputFileName = rosBagParserROGX(bagFileName, namespace_vec, varargin)
%ROSBAGPARSERROGX parse .bag file logged from ROG-X and save output in .mat file
%
% INPUT
% bagFileName:      string (without .bag extension) containing the name of
%                   logfile
% namespace_vec:    cell string vector containing the ROS namespaces of the
%                   drones (including the leading '/')
%
% Additional input arguments can be passed as a pair of (Name, Value):
% FixBagFile:       (optional, default false) boolean to enforce
%                   preprocessing of the bag file with the rosbag_fixer
%                   script
%
% OUTPUT
% outputFileName:   string containing full path to the .mat file containing
%                   parser output. The .mat file contains a struct for each
%                   of the namespaces indicated in the input argument
%                   namespace_vec (corresponding to the namespace of the
%                   drone) containing all the parsed data associated with
%                   that namespaces from the log.
%
% NOTATION
% The .bag file contains pose information expressed in the ENU frame
% (consistent with the ROS notation).
% In this parser, the information on pose (position and attitude) is
% converted to the NED frame (North, East, Down). Thus, the resulting .mat
% file contains positions expressed in NED and attitude in NED as well
% (that is, yaw angle is 0 deg when pointing to North and positive
% clockwise from above).
%
% AVAILABLE ROS TOPICS (more to be added):
%
% mavros/local_position/pose
% zed2i/zed_node/odom
% zed2i/zed_node/pose
% mavros/mocap/pose
%
% USAGE
% Example: to parse bag file 'X0899_2022-10-27-14-29-32.bag' obtained with drone '/rogx' from the current folder:
% rosBagParserROGX('X0899_2022-10-27-14-29-32', {'/rogx'})
%
% Example: to parse bag file '2021-07-16_10-40-27.bag' containing logged data of drones '/rogx2' and '/rogx3':
% put bag file in folder log_rosbag
% rosBagParserDrone('2021-07-16_10-40-27', {'/rogx2', '/rogx3'})
%
% Copyright ANT-X, 2023

%%% INPUT PARSING

pars = inputParser;

containsNoSpaces = @(x) isempty(regexp(x, '\s', 'once')); % true if string x does not contain any whitespaces

checkFileName = @(x) ischar(x) & containsNoSpaces(x);

%checkNamespace = @(x) ischar(x) & containsNoSpaces(x) & (x(1) == '/');
checkNamespaceVec = @(x) iscellstr(x);
checkFixBagFile = @(x) islogical(x);
defaultFixBagFile = false;

addRequired(pars, 'bagFileName', checkFileName);
addRequired(pars, 'namespace_vec', checkNamespaceVec);
addParameter(pars, 'FixBagFile', defaultFixBagFile, checkFixBagFile);

parse(pars, bagFileName, namespace_vec, varargin{:});

FLAG_FIX_BAG_FILE = pars.Results.FixBagFile;

%%% END INPUT PARSING

%dir_bag = 'log_rosbag';
%dir_parsed = 'log_mat';
% dir_bag = getRosbagLogPath();
% dir_parsed = getMatLogPath();

bagFileName_full = [bagFileName, '.bag'];

out_file = bagFileName;

N_ns = length(namespace_vec);

% run rosbag info
% TODO fix: if ROS has not been installed on the system (eg. if running the
% script from Windows), the rosbag info shell command will not be
% recognized and a warning will be issued.
fprintf('### EXTRACT INFO FROM LOG ###\n\n')
commandStr = ['rosbag info ', bagFileName_full];
[status, commandOut] = system(commandStr);
fprintf(commandOut);
fprintf('\n### DONE ###\n')

if FLAG_FIX_BAG_FILE
  bagFileName_full_raw = bagFileName_full;
  bagFileName_full_fixed = [bagFileName, '_fixed.bag'];

  % get directory containing this script
  str_file_path = mfilename('fullpath');
  [curDir, ~, ~] = fileparts(str_file_path);

  commandStr = [fullfile(curDir, 'fix_rosbag.sh'), ' ', ...
    bagFileName_full_raw, ' ', ...
    bagFileName_full_fixed];

  status = system(commandStr);
%   [status, commandOut] = system(commandStr);

  if status ~= 0
%     error(commandOut)
    error('An error occurred during fix of the bag file')
  end

  bagFileName_full = bagFileName_full_fixed; % run rosbag on the fixed script
end

r = rosbag(bagFileName_full);

topicList = r.AvailableTopics.Row;

fprintf('\n### TOPIC EXTRACTION ###\n\n');
parsedData = struct([]);

N = length(topicList);
assert(N > 0, 'No topics found in bag file.')

for ii = 1:N_ns
    namespace = namespace_vec{ii};
    fprintf('### Extracting topics for namespace %s ... ###\n\n', namespace);
    
    STR_ROS_NAMESPACE = [namespace, '/'];
    
    % remove slashes from namespace to be used as fields of the data
    % structure
    i_ns = regexp(namespace, '[^/]');
    ns_field = namespace(i_ns);
    
    % % TOPIC NAMES
    STR_TOPIC_POSITION = strcat(STR_ROS_NAMESPACE, 'mavros/local_position/pose');
    STR_TOPIC_ODOM = strcat(STR_ROS_NAMESPACE, 'zed2i/zed_node/odom');
    STR_TOPIC_ODOM_POSE = strcat(STR_ROS_NAMESPACE, 'zed2i/zed_node/pose');
    STR_TOPIC_MOCAP = strcat(STR_ROS_NAMESPACE, 'mavros/mocap/pose');
    
    % TODO add more topics
    
    % STR_TOPIC_VELOCITY = strcat(STR_ROS_NAMESPACE, 'local_position/velocity_body');
    % STR_TOPIC_POSITION_SETPOINT = strcat(STR_ROS_NAMESPACE, 'setpoint_raw/local');
    % STR_TOPIC_ATTITUDE_SETPOINT = strcat(STR_ROS_NAMESPACE, 'setpoint_raw/attitude');
    % STR_TOPIC_CONTROL_ACTIONS = strcat(STR_ROS_NAMESPACE, 'target_actuator_control');
    % STR_TOPIC_SENSORS_IMU = strcat(STR_ROS_NAMESPACE, 'imu/data_raw');
    % STR_TOPIC_MOCAP = strcat(STR_ROS_NAMESPACE, 'mocap/pose');
    % STR_TOPIC_LMNF_INJ = strcat(STR_ROS_NAMESPACE, 'LMNF_injection');
    % %STR_TOPIC_BATTERY = strcat(STR_ROS_NAMESPACE, 'battery');
    %
    %
    if isTopicLogged(topicList, STR_TOPIC_POSITION)
        fprintf('extracting topic %s ...\n', STR_TOPIC_POSITION)
        [s_pos, s_att, s_quat] = parse_topic_position(r, STR_TOPIC_POSITION);
        parsedData(1).(ns_field).estimated_pos = s_pos;
        parsedData(1).(ns_field).estimated_att = s_att;
        parsedData(1).(ns_field).estimated_quat = s_quat;
    end
    
    if isTopicLogged(topicList, STR_TOPIC_ODOM)
        fprintf('extracting topic %s ...\n', STR_TOPIC_ODOM)
        [s_pos, s_att, s_quat] = parse_topic_odom(r, STR_TOPIC_ODOM);
        parsedData(1).(ns_field).odom_pos = s_pos;
        parsedData(1).(ns_field).odom_att = s_att;
        parsedData(1).(ns_field).odom_quat = s_quat;
    end
    
    if isTopicLogged(topicList, STR_TOPIC_ODOM_POSE)
        fprintf('extracting topic %s ...\n', STR_TOPIC_ODOM_POSE)
        [s_pos, s_att, s_quat] = parse_topic_odom_pose(r, STR_TOPIC_ODOM_POSE);
        parsedData(1).(ns_field).odom_pose_pos = s_pos;
        parsedData(1).(ns_field).odom_pose_att = s_att;
        parsedData(1).(ns_field).odom_pose_quat = s_quat;
    end
    
    if isTopicLogged(topicList, STR_TOPIC_MOCAP)
        fprintf('extracting topic %s ...\n', STR_TOPIC_MOCAP)
        %     [s_mocap] = parse_topic_mocap(r, STR_TOPIC_MOCAP);
        %     parsedData(1).(ns_field).mocap_pos = s_mocap.pos;
        %     parsedData(1).(ns_field).mocap_att = s_mocap.att;
        %     parsedData(1).(ns_field).mocap_quat = s_mocap.quat;
        [s_pos, s_att, s_quat] = parse_topic_mocap(r, STR_TOPIC_MOCAP);
        parsedData(1).(ns_field).mocap_pos = s_pos;
        parsedData(1).(ns_field).mocap_att = s_att;
        parsedData(1).(ns_field).mocap_quat = s_quat;
    end
    
    %
    % if isTopicLogged(topicList, STR_TOPIC_VELOCITY)
    %     fprintf('extracting topic %s ...\n', STR_TOPIC_VELOCITY)
    %     [s_vel, s_rate] = parse_topic_velocity(r, STR_TOPIC_VELOCITY, s_quat);
    %     parsedData(1).(ns_field).vel = s_vel;
    %     parsedData(1).(ns_field).rate = s_rate;
    % end
    %
    % if isTopicLogged(topicList, STR_TOPIC_POSITION_SETPOINT)
    %     fprintf('extracting topic %s ...\n', STR_TOPIC_POSITION_SETPOINT)
    %     [s_pos0, s_vel0, s_yaw0, s_yawrate0] = parse_topic_position_setpoint(r, STR_TOPIC_POSITION_SETPOINT);
    %     parsedData(1).(ns_field).pos0 = s_pos0;
    %     parsedData(1).(ns_field).vel0 = s_vel0;
    %     parsedData(1).(ns_field).yaw0 = s_yaw0;
    %     parsedData(1).(ns_field).yawrate0 = s_yawrate0;
    % end
    %
    % if isTopicLogged(topicList, STR_TOPIC_ATTITUDE_SETPOINT)
    %     fprintf('extracting topic %s ...\n', STR_TOPIC_ATTITUDE_SETPOINT)
    %     [s_att0, s_quat0, s_thrust0, s_rate0] = parse_topic_attitude_setpoint(r, STR_TOPIC_ATTITUDE_SETPOINT);
    %     parsedData(1).(ns_field).att0 = s_att0;
    %     parsedData(1).(ns_field).quat0 = s_quat0;
    %     parsedData(1).(ns_field).thrust0 = s_thrust0;
    %     parsedData(1).(ns_field).rate0 = s_rate0;
    % end
    %
    % if isTopicLogged(topicList, STR_TOPIC_CONTROL_ACTIONS)
    %     fprintf('extracting topic %s ...\n', STR_TOPIC_CONTROL_ACTIONS)
    %     [s_mom, s_force] = parse_topic_control_actions(r, STR_TOPIC_CONTROL_ACTIONS);
    %     parsedData(1).(ns_field).mom = s_mom;
    %     parsedData(1).(ns_field).force = s_force;
    % end
    %
    % if isTopicLogged(topicList, STR_TOPIC_SENSORS_IMU)
    %     fprintf('extracting topic %s ...\n', STR_TOPIC_SENSORS_IMU)
    %     [s_acc, s_gyr] = parse_topic_sensors(r, STR_TOPIC_SENSORS_IMU);
    %     parsedData(1).(ns_field).acc = s_acc;
    %     parsedData(1).(ns_field).gyr = s_gyr;
    % end
    %
    %
    % if isTopicLogged(topicList, STR_TOPIC_LMNF_INJ)
    %     fprintf('extracting topic %s ...\n', STR_TOPIC_LMNF_INJ)
    %     [s_lmnf] = parse_topic_lmnf_inj(r, STR_TOPIC_LMNF_INJ);
    %     parsedData(1).(ns_field).lmnf = s_lmnf;
    % end
    
    % TODO fix: not working
    %     if isTopicLogged(topicList, STR_TOPIC_BATTERY)
    %         fprintf('extracting topic %s ...\n', STR_TOPIC_BATTERY)
    %         [s_bat] = parse_topic_battery(r, STR_TOPIC_BATTERY);
    %         parsedData(1).battery = s_bat;
    %     end
    
    fprintf('\n### Topic extraction for %s done. ###\n\n', namespace);
    
end


fprintf('\n### TOPIC EXTRACTION DONE.\n');

outputFileName = sprintf('%s.mat', out_file);

if exist(outputFileName, 'file') == 2
    delete(outputFileName)
end

save(outputFileName, '-struct', 'parsedData');

% cleanup temp files
if FLAG_FIX_BAG_FILE
    s_exp = [bagFileName, '_fixed*'];
    delete(s_exp);
end

fprintf('\nParsing done. Saving output to\n\n%s\n', outputFileName);

end

function [s_pos, s_att, s_quat] = parse_topic_position(rosBagObj, str_topic_name)

m = select(rosBagObj, 'Topic', str_topic_name);
ms = readMessages(m);

N = m.NumMessages;

s_pos = struct([]);
% s_pos(1).timestamp = zeros(N,1);
s_pos(1).timestamp = m.MessageList.Time - rosBagObj.StartTime;
s_pos(1).value = zeros(N,3);

s_att = struct([]);
% s_att(1).timestamp = zeros(N,1);
s_att(1).timestamp = m.MessageList.Time - rosBagObj.StartTime;
s_att(1).value = zeros(N,3);

s_quat = struct([]);
% s_quat(1).timestamp = zeros(N,1);
s_quat(1).timestamp = m.MessageList.Time - rosBagObj.StartTime;
s_quat(1).value = zeros(N,4);

for ii = 1:N
%     ts = ms{ii}.Header.Stamp;
%     s_pos.timestamp(ii) = ts.Sec + 1e-9 * ts.Nsec;
%     s_att.timestamp(ii) = ts.Sec + 1e-9 * ts.Nsec;
%     s_quat.timestamp(ii) = ts.Sec + 1e-9 * ts.Nsec;

    pos_enu = [ms{ii}.Pose.Position.X, ms{ii}.Pose.Position.Y, ms{ii}.Pose.Position.Z];
    pos_ned = [pos_enu(2), pos_enu(1), -pos_enu(3)];

    q_enu = [ms{ii}.Pose.Orientation.X; ms{ii}.Pose.Orientation.Y; ms{ii}.Pose.Orientation.Z; ms{ii}.Pose.Orientation.W];
    q_ned = utils.quaternionEnuToNed(q_enu);
    eul = quatToEuler(q_ned);

    s_pos.value(ii,:) = pos_ned;
    s_att.value(ii,:) = eul;
    s_quat.value(ii,:) = q_ned;
end

end

function [s_pos, s_att, s_quat] = parse_topic_odom(rosBagObj, str_topic_name)

m = select(rosBagObj, 'Topic', str_topic_name);
ms = readMessages(m);

N = m.NumMessages;

s_pos = struct([]);
% s_pos(1).timestamp = zeros(N,1);
s_pos(1).timestamp = m.MessageList.Time - rosBagObj.StartTime;
s_pos(1).value = zeros(N,3);

s_att = struct([]);
% s_att(1).timestamp = zeros(N,1);
s_att(1).timestamp = m.MessageList.Time - rosBagObj.StartTime;
s_att(1).value = zeros(N,3);

s_quat = struct([]);
% s_quat(1).timestamp = zeros(N,1);
s_quat(1).timestamp = m.MessageList.Time - rosBagObj.StartTime;
s_quat(1).value = zeros(N,4);

for ii = 1:N
%     ts = ms{ii}.Header.Stamp;
%     s_pos.timestamp(ii) = ts.Sec + 1e-9 * ts.Nsec;
%     s_att.timestamp(ii) = ts.Sec + 1e-9 * ts.Nsec;
%     s_quat.timestamp(ii) = ts.Sec + 1e-9 * ts.Nsec;

    pos_enu = [ms{ii}.Pose.Pose.Position.X, ms{ii}.Pose.Pose.Position.Y, ms{ii}.Pose.Pose.Position.Z];
    pos_ned = [pos_enu(2), pos_enu(1), -pos_enu(3)];

    q_enu = [ms{ii}.Pose.Pose.Orientation.X; ms{ii}.Pose.Pose.Orientation.Y; ms{ii}.Pose.Pose.Orientation.Z; ms{ii}.Pose.Pose.Orientation.W];
    q_ned = utils.quaternionEnuToNed(q_enu);
    eul = quatToEuler(q_ned);

    s_pos.value(ii,:) = pos_ned;
    s_att.value(ii,:) = eul;
    s_quat.value(ii,:) = q_ned;
end

end

function [s_pos, s_att, s_quat] = parse_topic_odom_pose(rosBagObj, str_topic_name)

m = select(rosBagObj, 'Topic', str_topic_name);
ms = readMessages(m);

N = m.NumMessages;

s_pos = struct([]);
% s_pos(1).timestamp = zeros(N,1);
s_pos(1).timestamp = m.MessageList.Time - rosBagObj.StartTime;
s_pos(1).value = zeros(N,3);

s_att = struct([]);
% s_att(1).timestamp = zeros(N,1);
s_att(1).timestamp = m.MessageList.Time - rosBagObj.StartTime;
s_att(1).value = zeros(N,3);

s_quat = struct([]);
% s_quat(1).timestamp = zeros(N,1);
s_quat(1).timestamp = m.MessageList.Time - rosBagObj.StartTime;
s_quat(1).value = zeros(N,4);

for ii = 1:N
%     ts = ms{ii}.Header.Stamp;
%     s_pos.timestamp(ii) = ts.Sec + 1e-9 * ts.Nsec;
%     s_att.timestamp(ii) = ts.Sec + 1e-9 * ts.Nsec;
%     s_quat.timestamp(ii) = ts.Sec + 1e-9 * ts.Nsec;

    pos_enu = [ms{ii}.Pose.Position.X, ms{ii}.Pose.Position.Y, ms{ii}.Pose.Position.Z];
    pos_ned = [pos_enu(2), pos_enu(1), -pos_enu(3)];

    q_enu = [ms{ii}.Pose.Orientation.X; ms{ii}.Pose.Orientation.Y; ms{ii}.Pose.Orientation.Z; ms{ii}.Pose.Orientation.W];
    q_ned = utils.quaternionEnuToNed(q_enu);
    eul = quatToEuler(q_ned);

    s_pos.value(ii,:) = pos_ned;
    s_att.value(ii,:) = eul;
    s_quat.value(ii,:) = q_ned;
end

end

function [s_pos, s_att, s_quat] = parse_topic_mocap(rosBagObj, str_topic_name)

m = select(rosBagObj, 'Topic', str_topic_name);
ms = readMessages(m);

N = m.NumMessages;

% s_mocap = struct([]);
% % s_acc(1).timestamp = zeros(N,1);
% s_mocap(1).timestamp = m.MessageList.Time - rosBagObj.StartTime;
% s_mocap(1).pos = zeros(N,3);
% s_mocap(1).quat= zeros(N,4);

s_pos = struct([]);
% s_pos(1).timestamp = zeros(N,1);
s_pos(1).timestamp = m.MessageList.Time - rosBagObj.StartTime;
s_pos(1).value = zeros(N,3);

s_att = struct([]);
% s_att(1).timestamp = zeros(N,1);
s_att(1).timestamp = m.MessageList.Time - rosBagObj.StartTime;
s_att(1).value = zeros(N,3);

s_quat = struct([]);
% s_quat(1).timestamp = zeros(N,1);
s_quat(1).timestamp = m.MessageList.Time - rosBagObj.StartTime;
s_quat(1).value = zeros(N,4);

for ii = 1:N
%     ts = ms{ii}.Header.Stamp;
%     s_acc.timestamp(ii) = ts.Sec + 1e-9 * ts.Nsec;
%     s_gyr.timestamp(ii) = ts.Sec + 1e-9 * ts.Nsec;

    pos_enu = [ms{ii}.Pose.Position.X, ms{ii}.Pose.Position.Y, ms{ii}.Pose.Position.Z];
    pos_ned = [pos_enu(2), pos_enu(1), -pos_enu(3)];

    q_enu = [ms{ii}.Pose.Orientation.X; ms{ii}.Pose.Orientation.Y; ms{ii}.Pose.Orientation.Z; ms{ii}.Pose.Orientation.W];
    q_ned = utils.quaternionEnuToNed(q_enu);
    eul = quatToEuler(q_ned);

%     s_mocap.pos(ii,:) = pos_ned;
%     s_mocap.att(ii,:) = eul;
%     s_mocap.quat(ii,:) = q_ned;

    s_pos.value(ii,:) = pos_ned;
    s_att.value(ii,:) = eul;
    s_quat.value(ii,:) = q_ned;

end

end

% function [s_vel, s_rate] = parse_topic_velocity(rosBagObj, str_topic_name, s_quaternion)
% 
% m = select(rosBagObj, 'Topic', str_topic_name);
% ms = readMessages(m);
% 
% N = m.NumMessages;
% 
% s_vel = struct([]);
% % s_vel(1).timestamp = zeros(N,1);
% s_vel(1).timestamp = m.MessageList.Time - rosBagObj.StartTime;
% s_vel(1).value = zeros(N,3);
% 
% s_rate = struct([]);
% % s_rate(1).timestamp = zeros(N,1);
% s_rate(1).timestamp = m.MessageList.Time - rosBagObj.StartTime;
% s_rate(1).value = zeros(N,3);
% 
% for ii = 1:N
% %     ts = ms{ii}.Header.Stamp;
% %     s_vel.timestamp(ii) = ts.Sec + 1e-9 * ts.Nsec;
% %     s_rate.timestamp(ii) = ts.Sec + 1e-9 * ts.Nsec;
% 
%     vel_flu = [ms{ii}.Twist.Linear.X, ms{ii}.Twist.Linear.Y, ms{ii}.Twist.Linear.Z];
%     vel_frd = [vel_flu(1), -vel_flu(2), -vel_flu(3)];
% 
%     % apply rotation from body frame to inertial frame
%     qi = interp1(s_quaternion.timestamp, s_quaternion.value, s_vel.timestamp(ii), 'previous');
%     A = quatToAtt(qi);
%     vel_ned = A' * vel_frd';
% 
%     rate_flu = [ms{ii}.Twist.Angular.X; ms{ii}.Twist.Angular.Y; ms{ii}.Twist.Angular.Z];
%     rate_frd = [rate_flu(1), -rate_flu(2), -rate_flu(3)];
% 
%     s_vel.value(ii,:) = vel_ned;
%     s_rate.value(ii,:) = rate_frd;
% end
% 
% end

% function [s_pos0, s_vel0, s_yaw0, s_yawrate0] = parse_topic_position_setpoint(rosBagObj, str_topic_name)
% 
% m = select(rosBagObj, 'Topic', str_topic_name);
% ms = readMessages(m);
% 
% N = m.NumMessages;
% 
% s_pos0 = struct([]);
% % s_x0(1).timestamp = zeros(N,1);
% s_pos0(1).timestamp = m.MessageList.Time - rosBagObj.StartTime;
% s_pos0(1).value = zeros(N,3);
% 
% s_vel0 = s_pos0;
% 
% s_yaw0 = struct([]);
% s_yaw0(1).timestamp = m.MessageList.Time - rosBagObj.StartTime;
% s_yaw0(1).value = zeros(N,1);
% 
% s_yawrate0 = s_yaw0;
% 
% for ii = 1:N
% %     ts = ms{ii}.Header.Stamp;
% %     s_x0.timestamp(ii) = ts.Sec + 1e-9 * ts.Nsec;
%     pos_enu = [ms{ii}.Position.X, ms{ii}.Position.Y, ms{ii}.Position.Z];
%     pos_ned = [pos_enu(2), pos_enu(1), -pos_enu(3)];
% 
%     vel_enu = [ms{ii}.Velocity.X, ms{ii}.Velocity.Y, ms{ii}.Velocity.Z];
%     vel_ned = [vel_enu(2), vel_enu(1), -vel_enu(3)];
% 
%     yaw_enu = ms{ii}.Yaw;
%     yaw_ned = pi/2 - yaw_enu;
% 
%     yawrate_enu = ms{ii}.YawRate;
%     yawrate_ned = -yawrate_enu;
% 
%     s_pos0.value(ii,:) = pos_ned;
%     s_vel0.value(ii,:) = vel_ned;
% 
%     s_yaw0.value(ii,:) = yaw_ned;
%     s_yawrate0.value(ii,:) = yawrate_ned;
% 
% %TODO
% end
% 
% end

% function [s_att0, s_quat0, s_thrust0, s_rate0] = parse_topic_attitude_setpoint(rosBagObj, str_topic_name)
% m = select(rosBagObj, 'Topic', str_topic_name);
% ms = readMessages(m);
% 
% N = m.NumMessages;
% 
% s_att0 = struct([]);
% s_att0(1).timestamp = m.MessageList.Time - rosBagObj.StartTime;
% s_att0(1).value = zeros(N,3);
% 
% s_quat0 = s_att0;
% s_quat0.value = zeros(N,4);
% 
% s_thrust0 = s_att0;
% s_thrust0.value = zeros(N,1);
% 
% s_rate0 = s_att0;
% 
% for ii = 1:N
%     q_enu = [ms{ii}.Orientation.X; ms{ii}.Orientation.Y; ms{ii}.Orientation.Z; ms{ii}.Orientation.W];
%     q_ned = utils.quaternionEnuToNed(q_enu);
%     eul = quatToEuler(q_ned);
%     thrust = ms{ii}.Thrust;
% 
%     bodyRate_flu =  [ms{ii}.BodyRate.X; ms{ii}.BodyRate.Y; ms{ii}.BodyRate.Z];
%     bodyRate_frd = utils.fluToFrd(bodyRate_flu);
% 
%     s_att0.value(ii,:) = eul;
%     s_quat0.value(ii,:) = q_ned;
%     s_thrust0.value(ii) = thrust;
%     s_rate0.value(ii,:) = bodyRate_frd;
% end
% 
% end

% function [s_mom, s_force] = parse_topic_control_actions(rosBagObj, str_topic_name)
% 
% m = select(rosBagObj, 'Topic', str_topic_name);
% ms = readMessages(m);
% 
% N = m.NumMessages;
% 
% s_mom = struct([]);
% % s_M(1).timestamp = zeros(N,1);
% s_mom(1).timestamp = m.MessageList.Time - rosBagObj.StartTime;
% s_mom(1).value = zeros(N,3);
% 
% s_force = s_mom;
% 
% for ii = 1:N
% %     ts = ms{ii}.Header.Stamp;
% %     s_M.timestamp(ii) = ts.Sec + 1e-9 * ts.Nsec;
% %     s_T.timestamp(ii) = ts.Sec + 1e-9 * ts.Nsec;
% 
% %     M = ms{ii}.Controls(2);
% %     T = ms{ii}.Controls(4);
% 
%     mom = ms{ii}.Controls(1:3);
%     force = ms{ii}.Controls(4:6);
% 
%     s_mom.value(ii,:) = mom;
%     s_force.value(ii,:) = force;
% end
% 
% % reorder force vector components: ZXY -> XYZ
% tmp = s_force.value;
% s_force.value = tmp(:, [2,3,1]);
% 
% end

% function [s_acc, s_gyr] = parse_topic_sensors(rosBagObj, str_topic_name)
% 
% m = select(rosBagObj, 'Topic', str_topic_name);
% ms = readMessages(m);
% 
% N = m.NumMessages;
% 
% s_acc = struct([]);
% % s_acc(1).timestamp = zeros(N,1);
% s_acc(1).timestamp = m.MessageList.Time - rosBagObj.StartTime;
% s_acc(1).value = zeros(N,3);
% 
% s_gyr = s_acc;
% 
% for ii = 1:N
% %     ts = ms{ii}.Header.Stamp;
% %     s_acc.timestamp(ii) = ts.Sec + 1e-9 * ts.Nsec;
% %     s_gyr.timestamp(ii) = ts.Sec + 1e-9 * ts.Nsec;
% 
%     accFLU = [ms{ii}.LinearAcceleration.X, ms{ii}.LinearAcceleration.Y, ms{ii}.LinearAcceleration.Z];
%     % convert to FRD
%     accFRD = [accFLU(1), -accFLU(2), -accFLU(3)];
% 
%     gyrFLU = [ms{ii}.AngularVelocity.X, ms{ii}.AngularVelocity.Y, ms{ii}.AngularVelocity.Z];
%     % convert to FRD
%     gyrFRD = [gyrFLU(1), -gyrFLU(2), -gyrFLU(3)];
% 
%     s_acc.value(ii,:) = accFRD;
%     s_gyr.value(ii,:) = gyrFRD;
% end
% 
% end

% function [s_lmnf] = parse_topic_lmnf_inj(rosBagObj, str_topic_name)
% 
% m = select(rosBagObj, 'Topic', str_topic_name);
% ms = readMessages(m);
% 
% N = m.NumMessages;
% 
% s_lmnf = struct([]);
% s_lmnf(1).timestamp = m.MessageList.Time - rosBagObj.StartTime;
% s_lmnf(1).L = zeros(N,1);
% s_lmnf(1).M = zeros(N,1);
% s_lmnf(1).N = zeros(N,1);
% s_lmnf(1).Fx = zeros(N,1);
% s_lmnf(1).Fy = zeros(N,1);
% s_lmnf(1).Fz = zeros(N,1);
% s_lmnf(1).openloop = logical(zeros(N,1));
% s_lmnf(1).enableL = logical(zeros(N,1));
% s_lmnf(1).enableM = logical(zeros(N,1));
% s_lmnf(1).enableN = logical(zeros(N,1));
% s_lmnf(1).enableFx = logical(zeros(N,1));
% s_lmnf(1).enableFy = logical(zeros(N,1));
% s_lmnf(1).enableFz = logical(zeros(N,1));
% 
% % from the mavros_antx/LMNFinjection documentation
% MASK_EN_L = uint8(1);
% MASK_EN_M = uint8(2);
% MASK_EN_N = uint8(4);
% MASK_EN_FX = uint8(8);
% MASK_EN_FY = uint8(16);
% MASK_EN_FZ = uint8(32);
% 
% for ii = 1:N
%     msk = ms{ii}.Mask;
%     L = ms{ii}.L;
%     M = ms{ii}.M;
%     N = ms{ii}.N;
% 
%     Fx = ms{ii}.F(1);
%     Fy = ms{ii}.F(2);
%     Fz = ms{ii}.F(3);
% 
%     ol = ms{ii}.TypeOpenloop;
% 
%     enableL = bitand(MASK_EN_L, msk) > 0;
%     enableM = bitand(MASK_EN_M, msk) > 0;
%     enableN = bitand(MASK_EN_N, msk) > 0;
%     enableFx = bitand(MASK_EN_FX, msk) > 0;
%     enableFy = bitand(MASK_EN_FY, msk) > 0;
%     enableFz = bitand(MASK_EN_FZ, msk) > 0;
% 
% 
%     s_lmnf.L(ii) = L;
%     s_lmnf.M(ii) = M;
%     s_lmnf.N(ii) = N;
%     s_lmnf.Fx(ii) = Fx;
%     s_lmnf.Fy(ii) = Fy;
%     s_lmnf.Fz(ii) = Fz;
% 
%     s_lmnf.openloop(ii) = ol;
% 
%     s_lmnf.enableL(ii) = enableL;
%     s_lmnf.enableM(ii) = enableM;
%     s_lmnf.enableN(ii) = enableN;
%     s_lmnf.enableFx(ii) = enableFx;
%     s_lmnf.enableFy(ii) = enableFy;
%     s_lmnf.enableFz(ii) = enableFz;
% end
% 
% end

% function [s_bat] = parse_topic_battery(rosBagObj, str_topic_name)
% % TODO fix: the readMessages function fails for some reason
% 
% m = select(rosBagObj, 'Topic', str_topic_name);
% ms = readMessages(m);
% 
% N = m.NumMessages;
% 
% s_bat = struct([]);
% % s_acc(1).timestamp = zeros(N,1);
% s_bat(1).timestamp = m.MessageList.Time - rosBagObj.StartTime;
% s_bat(1).voltage = zeros(N,1);
% s_bat(1).current = zeros(N,1);
% 
% for ii = 1:N
% %     ts = ms{ii}.Header.Stamp;
% %     s_acc.timestamp(ii) = ts.Sec + 1e-9 * ts.Nsec;
% %     s_gyr.timestamp(ii) = ts.Sec + 1e-9 * ts.Nsec;
% 
%     s_bat.voltage(ii) = ms{ii}.Voltage;
%     s_bat.current(ii) = ms{ii}.Current;
% 
% end
% 
% end

function isLogged = isTopicLogged(loggedTopicNames, STR_NAME_TOPIC)
isLogged = false;

for i = 1:length(loggedTopicNames)
    ss = loggedTopicNames{i};
    if strcmp(STR_NAME_TOPIC, ss)
        isLogged = true;
    end
end
end
