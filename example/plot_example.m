%PLOT_EXAMPLE Plot data from a parsed sample log file.

clc
clear
close all

%% load parsed log data
s = 'X0899_2022-10-27-14-29-32.mat';

r = load(s);

%% plot odometry vs onboard position estimate
figure

subplot 311
stairs(r.rogx.estimated_pos.timestamp, r.rogx.estimated_pos.value(:,1))
hold on
% plot(r.rogx.odom.timestamp, r.rogx.odom.value(:,1))
plot(r.rogx.odom_pose_pos.timestamp, r.rogx.odom_pose_pos.value(:,1))
grid
ylabel('N [m]')
title('Position: compare estimate vs odometry')

subplot 312
stairs(r.rogx.estimated_pos.timestamp, r.rogx.estimated_pos.value(:,2))
hold on
% plot(r.rogx.odom.timestamp, r.rogx.odom.value(:,2))
plot(r.rogx.odom_pose_pos.timestamp, r.rogx.odom_pose_pos.value(:,2))
grid
ylabel('E [m]')

subplot 313
stairs(r.rogx.estimated_pos.timestamp, r.rogx.estimated_pos.value(:,3))
hold on
% plot(r.rogx.odom.timestamp, r.rogx.odom.value(:,3))
plot(r.rogx.odom_pose_pos.timestamp, r.rogx.odom_pose_pos.value(:,3))
ylabel('D [m]')
grid
legend('estimate', 'odometry')
xlabel('time [s]')

%% plot mocap position
figure
plot(r.rogx.mocap_pos.timestamp, r.rogx.mocap_pos.value)
grid
title('Position: mocap')
legend('N','E','D')
ylabel('[m]')
xlabel('time [s]')

%% plot mocap attitude
figure
plot(r.rogx.mocap_att.timestamp, r.rogx.mocap_att.value/pi*180)
grid
title('Attitude: mocap')
legend('roll','pitch','yaw')
xlabel('time [s]')
ylabel('[deg]')

%% plot odometry vs onboard attitude estimate
figure

subplot 311
plot(r.rogx.estimated_att.timestamp, r.rogx.estimated_att.value(:,1)*180/pi)
hold on
% plot(r.rogx.odom_pos.timestamp, r.rogx.odom_att.value(:,1)*180/pi)
plot(r.rogx.odom_pose_att.timestamp, r.rogx.odom_pose_att.value(:,1)*180/pi)
grid
ylabel('roll [deg]')
title('Attitude: compare estimate vs odometry')

subplot 312
plot(r.rogx.estimated_att.timestamp, r.rogx.estimated_att.value(:,2)*180/pi)
hold on
% plot(r.rogx.odom_pos.timestamp, r.rogx.odom_att.value(:,2)*180/pi)
plot(r.rogx.odom_pose_att.timestamp, r.rogx.odom_pose_att.value(:,2)*180/pi)
grid
ylabel('pitch [deg]')

subplot 313
plot(r.rogx.estimated_att.timestamp, r.rogx.estimated_att.value(:,3)*180/pi)
hold on
% plot(r.rogx.odom_pos.timestamp, r.rogx.odom_att.value(:,3)*180/pi)
plot(r.rogx.odom_pose_att.timestamp, r.rogx.odom_pose_att.value(:,3)*180/pi)
ylabel('yaw [deg]')
grid
legend('estimate', 'odometry')
xlabel('time [s]')

%% APPLY CORRECTION TO ODOM
% the odometry position and attitude are initialized at zero when the
% odometry algorithm is started (that is, the "local East" axis is aligned
% with the body X axis at the initial time instant).
% On the other hand, mocap position and attitude are referred to the local
% inertial mocap ENU frame.
% In order to compare odometry vs mocap information we have to apply
% a rotation and a translation to the odometry data.

%% get position and yaw from mocap at initial time instant
x0_mocap = r.rogx.mocap_pos.value(1,1);
y0_mocap = r.rogx.mocap_pos.value(1,2);
z0_mocap = r.rogx.mocap_pos.value(1,3);

psi0_mocap = r.rogx.mocap_att.value(1,3);

psi0_odom = r.rogx.odom_pose_att.value(1,3);

%% compute difference in heading (yaw) between odometry and mocap at initial time instant
delta_psi0 = psi0_odom - psi0_mocap;

%% apply rotation (body frame->NED)
R_delta_psi0 = getAttitudeMatrix('z', delta_psi0);

odom_pose_pos = r.rogx.odom_pose_pos.value;
odom_pose_pos_rotated = (R_delta_psi0 * odom_pose_pos')';

% % debug
% figure
% plot(r.rogx.odom_pose_pos.timestamp, odom_pose_pos_rotated)

%% get position from odom at initial time instant (after rotation)
x0_odom = odom_pose_pos_rotated(1,1);
y0_odom = odom_pose_pos_rotated(1,2);
z0_odom = odom_pose_pos_rotated(1,3);

%% compute difference in position (NED) between odometry and mocap at initial time instant
delta_pos0 = [x0_mocap - x0_odom, ...
    y0_mocap - y0_odom, ...
    z0_mocap - z0_odom];

%% apply linear translation (odom local frame->mocap frame)
N = length(r.rogx.odom_pose_pos.timestamp);
% delta_pos = ones(N,1) * [x0_mocap, y0_mocap, z0_mocap];
delta_pos = ones(N,1) * delta_pos0;
odom_pose_pos_ned = odom_pose_pos_rotated + delta_pos;

% % debug
% figure
% plot(r.rogx.odom_pose_pos.timestamp, odom_pose_pos_ned)

%%
figure

subplot 311
% stairs(r.rogx.estimated_pos.timestamp, r.rogx.estimated_pos.value(:,1))
% hold on
% plot(r.rogx.odom.timestamp, r.rogx.odom.value(:,1))
plot(r.rogx.odom_pose_pos.timestamp, odom_pose_pos_ned(:,1))
hold on
plot(r.rogx.mocap_pos.timestamp, r.rogx.mocap_pos.value(:,1))
grid
ylabel('N [m]')
title('Position: comparison between odometry and ground truth')

subplot 312
% stairs(r.rogx.estimated_pos.timestamp, r.rogx.estimated_pos.value(:,2))
% hold on
% plot(r.rogx.odom.timestamp, r.rogx.odom.value(:,2))
plot(r.rogx.odom_pose_pos.timestamp, odom_pose_pos_ned(:,2))
hold on
plot(r.rogx.mocap_pos.timestamp, r.rogx.mocap_pos.value(:,2))
grid
ylabel('E [m]')

subplot 313
% stairs(r.rogx.estimated_pos.timestamp, r.rogx.estimated_pos.value(:,3))
% hold on
% plot(r.rogx.odom.timestamp, r.rogx.odom.value(:,3))
% plot(r.rogx.odom_pose_pos.timestamp, r.rogx.odom_pose_pos.value(:,3))
plot(r.rogx.odom_pose_pos.timestamp, odom_pose_pos_ned(:,3))
hold on
plot(r.rogx.mocap_pos.timestamp, r.rogx.mocap_pos.value(:,3))
ylabel('D [m]')
grid
legend('odometry', 'mocap')
xlabel('time [s]')
