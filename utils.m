classdef utils
    % This class contains all the extra functions. E.g.: conversion
    % functions between different frames.
    
    % Copyright ANT-X, 2020
    
    methods(Static)
        function enu = positionNedToEnu(ned)
            % Converts a position vector defined in a NED frame to the same
            % position in an ENU frame.
            % @param ned the position in the NED frame.
            % @return enu the position in the ENU frame.
            enu = [ned(2), ned(1), -ned(3)];
        end
        
        function ned = positionEnuToNed(enu)
            % Converts a position vector defined in an ENU frame to the same
            % position in a NED frame.
            % @param enu the position in the ENU frame.
            % @return ned the position in the NED frame.
            ned = utils.positionNedToEnu(enu);
        end
        
        function flu = frdToFlu(frd)
            % Converts a vector defined in the Forward-Right-Down body
            % frame to the same vector in the Forward-Left-Up body frame.
            % @param frd the vector defined in the FRD body frame.
            % @return flu the vector defined in the FLU body frame.
            flu = [frd(1) -frd(2) -frd(3)];
        end
        
        function frd = fluToFrd(flu)
            % Converts a vector defined in the Forward-Left-Up body
            % frame to the same vector in the Forward-Right-Down body frame.
            % @param flu the vector defined in the FLU body frame.
            % @return frd the vector defined in the FRD body frame.
            frd = utils.frdToFlu(flu);
        end
        
        function quatEnu = quaternionNedToEnu(quatNed)
            phi = pi;
            qx = [sin(phi/2) ;
                      0      ;
                      0      ;
                  cos(phi/2)];
              
            psi = -pi/2;
            qz = [    0      ;
                      0      ;
                  sin(psi/2) ;
                  cos(psi/2)];
               
            q_baselink_to_aircraft = qx;
            q_enu_to_ned = quatProd( qz, qx );
            
            quat = quatProd( quatConj( q_baselink_to_aircraft ), quatProd( quatNed, quatConj( q_enu_to_ned ) ) );
            norm = sqrt(quat(1)^2 + quat(2)^2 + quat(3)^2 + quat(4)^2);
            quatEnu = quat / norm;
        end
        
        function quatNed = quaternionEnuToNed(quatEnu)
            phi = pi;
            qx = [sin(phi/2) ;
                      0      ;
                      0      ;
                  cos(phi/2)];
              
            psi = -pi/2;
            qz = [    0      ;
                      0      ;
                  sin(psi/2) ;
                  cos(psi/2)];
               
            q_baselink_to_aircraft = qx;
            q_enu_to_ned = quatProd( qz, qx );
            
            quat = quatProd( q_baselink_to_aircraft, quatProd( quatEnu, q_enu_to_ned ) );
            norm = sqrt(quat(1)^2 + quat(2)^2 + quat(3)^2 + quat(4)^2);
            quatNed = quat / norm;
        end
    end
end